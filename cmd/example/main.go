package main

import (
	"log"
	"net/http"

	"github.com/usvc/go-observe/pkg/metrics"
)

func main() {
	mux := http.NewServeMux()
	metrics.AddHandler(mux)
	http.ListenAndServe(
		"0.0.0.0:12345",
		applyMiddlewares(
			mux,
			observeRequestDuration,
			observeRequestStatus,
			observeResponseSize,
		),
	)
}

func applyMiddlewares(app http.Handler, middlewares ...middleware) http.Handler {
	for i := 0; i < len(middlewares); i++ {
		log.Println("a")
		app = middlewares[i](app)
	}
	return app
}

type middleware func(http.Handler) http.Handler

func observeRequestDuration(next http.Handler) http.Handler {
	duration := metrics.NewRequestDuration()
	return duration.Apply(next)
}

func observeResponseSize(next http.Handler) http.Handler {
	status := metrics.NewResponseSize()
	return status.Apply(next)
}

func observeRequestStatus(next http.Handler) http.Handler {
	size := metrics.NewRequestStatus()
	return size.Apply(next)
}
