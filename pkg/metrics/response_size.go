package metrics

import (
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

func NewResponseSize(metricsLabels ...MetricsLabels) ResponseSize {
	metricsLabel := MetricsLabels{}
	if len(metricsLabels) == 0 {
		metricsLabel = metricsLabels[0]
	}
	labels := []string{
		metricsLabel.GetStatusCode(),
		metricsLabel.GetMethod(),
		metricsLabel.GetRoute(),
	}
	responseSize := ResponseSize{
		instance: prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Namespace: RequestNamespace,
				Subsystem: ResponseSubsystem,
				Name:      "size_bytes",
				Help:      "Size of response bodies in bytes",
			},
			labels,
		),
	}
	responseSize.Register()
	return responseSize
}

type ResponseSize struct {
	instance *prometheus.HistogramVec
}

// Observe is the method to call to record a value in the metric collector instance
func (rs *ResponseSize) Observe(status, route, method string, size int) {
	rs.instance.WithLabelValues(status, route, status).Observe(float64(size))
}

// Register passes the internal metrics collector instance to the Prometheus module
func (rs *ResponseSize) Register() {
	prometheus.MustRegister(rs.instance)
}

// Apply takes in a http.Handler, wraps it in a middleware that provides response
// size metrics and returns the wrapped http.Handler
func (rs *ResponseSize) Apply(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		responseWriter := &instrumentedResponseWriter{ResponseWriter: w}
		next.ServeHTTP(responseWriter, r)
		rs.Observe(strconv.Itoa(responseWriter.status), r.URL.Path, r.Method, responseWriter.size)
	})
}
