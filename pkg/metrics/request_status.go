package metrics

import (
	"net/http"
	"strconv"

	"github.com/prometheus/client_golang/prometheus"
)

// NewRequestStatus returns a metric instance
func NewRequestStatus(metricsLabels ...MetricsLabels) RequestStatus {
	metricsLabel := MetricsLabels{}
	if len(metricsLabels) == 0 {
		metricsLabel = metricsLabels[0]
	}
	labels := []string{
		metricsLabel.GetStatusCode(),
		metricsLabel.GetMethod(),
		metricsLabel.GetRoute(),
	}
	requestStatus := RequestStatus{
		instance: prometheus.NewCounterVec(
			prometheus.CounterOpts{
				Namespace: RequestNamespace,
				Subsystem: RequestSubsystem,
				Name:      "status_code",
				Help:      "Status codes of handled requests segregated by routes",
			},
			labels,
		),
	}
	requestStatus.Register()
	return requestStatus
}

type RequestStatus struct {
	instance *prometheus.CounterVec
}

// Observe is the method to call to record a value in the metric collector instance
func (rs *RequestStatus) Observe(status, route, method string) {
	rs.instance.WithLabelValues(route, status).Inc()
}

// Register passes the internal metrics collector instance to the Prometheus module
func (rs *RequestStatus) Register() {
	prometheus.MustRegister(rs.instance)
}

// Apply takes in a http.Handler, wraps it in a middleware that provides status metrics
// and returns the wrapped http.Handler
func (rs *RequestStatus) Apply(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		responseWriter := &instrumentedResponseWriter{ResponseWriter: w}
		next.ServeHTTP(responseWriter, r)
		rs.Observe(strconv.Itoa(responseWriter.status), r.URL.Path, r.Method)
	})
}
