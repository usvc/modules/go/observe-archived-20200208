package metrics

import (
	"net/http"
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

var requestDurationBuckets = []float64{
	0, 10, 25, 50, 75, 100, 250, 500, 750, 1000, 1500, 2000, 2500, 3000, 3500, 4000, 4500, 5000, 6000, 7500, 10000, 15000, 20000, 30000,
}

// NewRequestDuration creates a new metric instance to measure
// the latency of incoming requests
func NewRequestDuration(metricsLabels ...MetricsLabels) RequestDuration {
	metricsLabel := MetricsLabels{}
	if len(metricsLabels) == 0 {
		metricsLabel = metricsLabels[0]
	}
	labels := []string{
		metricsLabel.GetMethod(),
		metricsLabel.GetRoute(),
	}
	requestDuration := RequestDuration{
		instance: prometheus.NewHistogramVec(
			prometheus.HistogramOpts{
				Namespace: RequestNamespace,
				Subsystem: RequestSubsystem,
				Name:      "duration_ms",
				Help:      "Duration in milliseconds of handled requests",
				Buckets:   requestDurationBuckets,
			},
			labels,
		),
	}
	requestDuration.Register()
	return requestDuration
}

type RequestDuration struct {
	instance *prometheus.HistogramVec
}

// Observe is the method to call to record a value in the metric collector instance
func (rd *RequestDuration) Observe(route string, timeSince time.Duration) {
	rd.instance.WithLabelValues(route).Observe(float64(timeSince.Microseconds()) / 1000)
}

// Register passes the internal metrics collector instance to the Prometheus module
func (rd *RequestDuration) Register() {
	prometheus.MustRegister(rd.instance)
}

// Apply takes in a http.Handler, wraps it in a middleware that provides request
// duration metrics and returns the wrapped http.Handler
func (rd *RequestDuration) Apply(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		startedAt := time.Now()
		next.ServeHTTP(w, r)
		rd.Observe(r.URL.Path, time.Since(startedAt))
	})
}
