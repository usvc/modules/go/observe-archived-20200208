package metrics

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func AddHandler(handler *http.ServeMux) {
	handler.Handle("/metrics", promhttp.Handler())
}
