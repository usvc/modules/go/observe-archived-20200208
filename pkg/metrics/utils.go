package metrics

import "net/http"

const (
	DefaultLabelMethod     = "method"
	DefaultLabelRoute      = "route"
	DefaultLabelStatusCode = "status_code"
	ResponseSubsystem      = "response"
	RequestNamespace       = "http"
	RequestSubsystem       = "request"
)

type MetricsLabels struct {
	Method     string
	Route      string
	StatusCode string
}

func (ml MetricsLabels) GetMethod() string {
	if len(ml.Method) > 0 {
		return ml.Method
	}
	return DefaultLabelMethod
}

func (ml MetricsLabels) GetRoute() string {
	if len(ml.Route) > 0 {
		return ml.Route
	}
	return DefaultLabelRoute
}

func (ml MetricsLabels) GetStatusCode() string {
	if len(ml.StatusCode) > 0 {
		return ml.StatusCode
	}
	return DefaultLabelStatusCode
}

type instrumentedResponseWriter struct {
	http.ResponseWriter
	status int
	size   int
}

func (s *instrumentedResponseWriter) Write(data []byte) (int, error) {
	s.size += len(data)
	return s.ResponseWriter.Write(data)
}

func (s *instrumentedResponseWriter) WriteHeader(status int) {
	s.status = status
	s.ResponseWriter.WriteHeader(status)
}
