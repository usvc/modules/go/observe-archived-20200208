# Observe

The `go-observe` package provides observability functionality to a cloud-native service.

# Usage

## Importing

```go
// for metrics
import "github.com/usvc/go-observe/pkg/metrics"
```

## Usage

`go-observe` is meant for use with the standard `http.Handler` as middlewares. The following code samples assume that a variable named `mux` that implements the `http.Handler` interface is defined.

### Request Duration Instrumentation

#### As HTTP Middleware

```go
duration := metrics.NewRequestDuration()
mux = duration.Apply(mux)
```

#### As Standalone

```go
duration := metrics.NewRequestDuration()
// ... start the timer somewhere ...
startedAt := time.Now()
// ... and later ...
duration.Observe(r.URL.Path, time.Since(startedAt))
```

### Request Status Instrumentation

#### As HTTP Middleware

```go
status := metrics.NewRequestStatus()
mux = status.Apply(mux)
```

#### As Standalone

```go
status := metrics.NewRequestStatus()
// ... somewhere after the status code is set ...
status.Observe(strconv.Itoa(http.StatusOK), r.URL.Path, r.Method)
```

### Response Size Instrumentation

#### As HTTP Middleware

```go
size := metrics.NewResponseSize()
mux = size.Apply(mux)
```

#### As Standalone

```go
size := metrics.NewResponseSize()
// ... somehwere after the response has been decided ...
size.Observe(strconv.Itoa(http.StatusOK), r.URL.Path, r.Method, len(body))
```

# Examples

A server that implements all metrics as middlewares can be found in [the example binary directory at ./cmd/example](./cmd/example).

# Development Runbook

## Continuous Integration

`TODO`

# License

This package is licensed under the MIT license. Full text can be found in [the LICENSE file](./LICENSE).
